# Cucumber Tests

## Run all tests
    $ cucumber-js

## Verify A Specific Feature
    $ cucumber-js features/cucumber_testing.feature

## Tags
### Run Orvis Tests
    $ cucumber-js --tags @orvis

### Run Tests That Are Not Slow
    @ cucumber-js --tags ~@slow
