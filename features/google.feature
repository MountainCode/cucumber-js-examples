Feature: Google Search
  In order to more discover relevant websites
  As an Internet user
  I want to be able to find pages through google

  @slow
  Scenario: Simple search
    Given I go to google.com
    When I do a search for "tobi nodejs"
    Then I should be shown a link to https://github.com/LearnBoost/tobi
