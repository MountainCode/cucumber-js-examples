Feature: Orvis Keyword Search
  In order to more easily find the product I'm looking for
  As a shopper
  I want to be able to find products with a keyword search

  @slow @orvis
  Scenario: Search for polo shirt
    Given I go to www.orvis.com
    When I do an Orvis keyword search for "signature polo"
    Then I should be shown a thumbnail for product 4A5T

  @slow @orvis
  Scenario: Search for dog bed
    Given I go to www.orvis.com
    When I do an Orvis keyword search for "dog bed"
    Then I should be shown a thumbnail for product 2E06
