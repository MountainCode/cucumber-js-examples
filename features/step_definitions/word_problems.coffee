wordProblemStepDefinintions = () ->
  apple_count = 0
  @Given /^\w+ has (\d+) apples?$/, (num, callback) ->
    apple_count = num
    callback()

  @When /^s?he gives (\d+) apples? to \w+$/, (num, callback) ->
    apple_count -= num
    callback()

  @Then /^s?he should have (\d+) apples? left$/, (num, callback) ->
    if parseInt(num) is apple_count
      callback()
    else
      throw new Error("Found #{apple_count} apples, but expected #{num}")

module.exports = wordProblemStepDefinintions
