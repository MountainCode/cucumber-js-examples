tobi = require 'tobi'
should = require 'should'

internetStepDefinintions = () ->
  browser = null
  links = null

  @Given /^I go to (.*)$/, (url, callback) ->
    browser = tobi.createBrowser 80, url
    callback()

  @When /^I do a search for "([^"]*)"$/, (search_term, callback) ->
    browser.get '/', (res, $) ->
      $('form').fill
        q: search_term
      .submit (res, $) ->
        links = ($('.g a').map () ->
          @.href?.replace /^\/url\?q=([^&]*).*$/, '$1'
        .get())
        callback()

  @When /^I do an Orvis keyword search for "([^"]*)"$/, (search_term, callback) ->
    browser.get '/', (res, $) ->
      $('#searchform').fill
        keyword: search_term
      .submit (res, $) ->
        links = ($('#PageContent a').map () ->
          @.href
        .get())
        callback()

  @Then /^I should be shown a thumbnail for product (.*)$/, (pf_id, callback) ->
    links.should.include "/store/product.aspx?pf_id=#{pf_id}"
    callback()

  @Then /^I should be shown a link to (.*)$/, (href, callback) ->
    links.should.include href
    callback()

module.exports = internetStepDefinintions
