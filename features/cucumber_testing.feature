Feature: Cucumber Testing
  In order to develop more reliable software with live readable software
  As a software developer
  I want to be able to test my JavaScript with Cucumber

  Scenario: run a simple test
    Given Sue has 3 apples
    When she gives 1 apple to Johnny
    Then she should have 2 apples left
